# Context: 
Deployment of a Swagger Specification onto the Apigee Developer Portal is a multi step process. The complexity of those steps is abstracted by a single command offered by this library. 

**Note:** This library relies on standard Apigee Edge REST API's and does **not** use the Smart Docs API.The Smart Docs API requires the installation of a custom Drupal Module which may not be feasible in all scenarios.

# Known Limitations: 
This library registers the Swagger Specification onto the Developer Portal but does **not** publish it for consumer consumption. Rendering & Publishing the swagger specification (making it visible to API consumers) needs to be performed manually. This manual step typically involves selecting a subset of registered API's and clicking 'Publish'. The automated publish capability is not supported because the capability is not offered via REST API's on the Edge Platform. 

# Intended Usage(Command Line):
```gradle
./gradlew publishToDevPortal -DjsonFilePath=/Users/Rahul/dev/2016-sandbox/workspace/apigee-dev-portal-registrator/examples/petstore-simple.json -DspecQualifier=master
```
Note: 
- jsonFilePath : The absolute file path of the swagger specification which needs to be published
- specQualifier: An arbitrary qualifier (develop/master/release/alpha/beta/live)

The library is expected to be used from within Jenkins to publish Swagger Specification to Developer Portal while publishing the Proxy Bundle to the Apigee Edge Server(separate capability)

# Testing the API locally
 - `git clone https://scm.lhstaging.com/digital-integration/apigee-dev-portal-registrator.git`
 - Navigate to the directory where you cloned the project
```gradle
./gradlew publishToDevPortal -DjsonFilePath=/Users/Rahul/dev/2016-sandbox/workspace/apigee-dev-portal-registrator/examples/petstore-simple.json -DspecQualifier=master
```
 - Edit the publisher-config.yml in the examples directory.
 - Ensure that your YAML has the following sections defined accurately (remaining sections are redundant in this context)
 
             - Apigee Organization
             - Credentials to Developer Portal & Apigee Edge Server
             - URL's to Dev Portal & Apigee Edge Server
 
 
 ```yaml
 
  -
  &apigeeEdgeServerCredentials
  name: apigeeEdgeServerCredentials
  properties: { description: 'Apigee Edge Server Credentials', username: 'rahul.mishra79@gmail.com' , password: '' }

 -
  &apigeeDevPortalCredentials
  name: apigeeDevPortalCredentials
  properties: { description: 'Apigee Dev Portal Credentials', username: 'rahul' , password: '' }
  
  apigee:
 organization: rahulmishra
 edgeServer:
  baseUrl: 'https://api.enterprise.apigee.com/v1'
  credentialRef: *apigeeEdgeServerCredentials
 devPortal:
  baseUrl: 'http://dev-rahulmishara.devportal.apigee.com'
  credentialRef: *apigeeDevPortalCredentials
  smartDocQueueProcessorUrl: '/cron.php' #This is the Cron Job URL as seen in Configuration > System > Cron #Ignore this key for now
  smartDocQueueProcessorParams: [ 'cron_key' : 'iQAwtHZkMkyx491f_E_khh-3xKPi8bQnaSIk4lG7AzY' ] #Ignore this key for now
 
 ```
 - Ensure that you copy the full YAML file(or copy the anchors and aliases correctly), make edits as shown above . 
 - Validate your YAML file using an [Online YAML Validator]
 - If the YAML file is valid, Run the gradle task publishToDevPortal as shown above
 
 
[jenkins-master-config]: https://scm.lhstaging.com/digital-integration/lh-jenkins/blob/feature/persistenceChangesAfterHZNDrop1/conf/jenkins-master-config.yml
[Online YAML Validator]: http://yaml-online-parser.appspot.com/ 
