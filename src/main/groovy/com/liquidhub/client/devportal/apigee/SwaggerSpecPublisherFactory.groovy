package com.liquidhub.client.devportal.apigee

import com.liquidhub.client.devportal.apigee.config.ApigeeConnectivityConfig
import com.liquidhub.client.devportal.apigee.model.SwaggerJsonFile
import com.liquidhub.framework.config.impl.YAMLConfigurationLoader as finder
import com.liquidhub.jenkins.support.credentials.Credentials

/**
 * Factory Method style interface to publish a swagger specification onto the Developer Portal
 *
 * @author Rahul Mishra, LiquidHub
 */
class SwaggerSpecPublisherFactory {

    /**
     * Registers a Swagger Spec Revision onto the Developer Portal. The API Model information is constructed using the Swagger Spec Info Section
     *
     * @param An instance of a SwaggerJSONFile
     */

    static void registerSwaggerSpecRevision(SwaggerJsonFile file, Credentials edgeServerCredentials, Credentials devPortalCredentials=null) {
		
        ApigeeConnectivityConfig config = finder.findConfigurationForKey(CONFIG_KEY, System.env[FILE_PATH_KEY])
		
		devPortalCredentials = new Credentials("arun_singh@horizonblue.com", "ButterB19");
		
        config.edgeServer.credentials = edgeServerCredentials
        config.devPortal.credentials = devPortalCredentials

        if (!config) {
            throw new RuntimeException("Could not create an instance of ApigeeDevPortalClient, no credentials, org or management server host specified")
        }

        SwaggerSpecPublisher client = new SwaggerSpecPublisher(config)
        client.registerSwaggerSpecRevision(file)


    }

	static void processSwagger(swagger){
		
		 try {
			def ws=System.env["WORKSPACE"]
			println ws
			ws=ws.substring(0, ws.lastIndexOf('\\'))
			def jsonFilePath = ws+'/apiswaggers/swaggers/'+swagger
				
			def specQualifier =  System.getProperty('specQualifier')
			def templateFilePath = System.getProperty('templateFilePath')

			println jsonFilePath
			println specQualifier
			println templateFilePath
			
			SwaggerJsonFile file = new SwaggerJsonFile(jsonFilePath, specQualifier)
			file.addTemplate("devportal-drupal-cms", "Method", templateFilePath)


			SwaggerSpecPublisherFactory.registerSwaggerSpecRevision(file, new Credentials("arun_singh@horizonblue.com", "ButterB19"))
		} catch (Exception e) {
           println(e.getStackTrace()); 
        }
	}
    public static void main(String[] args) {
		
		println 'I am called'
		
		def swaggers = System.getProperty('swaggers')
		println swaggers
		swaggers.split(',').each { swagger ->
			processSwagger(swagger);
		}


        // DevPortalClientFactory.getInstance().createOrUpdateModelTemplate("namev1", "/Users/Rahul/dev/templatejson.txt", "method", )
        //DevPortalClientFactory.instance.createSwaggerSpecRevision("namev1", "/Users/Rahul/dev/petstore-simple.json")
        //	println DevPortalClientFactory.getInstance().listAPIModelsInOrg()
    }

    //This is the config key we search in the YAML file which is being loaded
    private static final String CONFIG_KEY = 'apigee'

    //We expect a environment variable with this key, the value of that key MUST be a YAML file which contains the above config key
    private static final String FILE_PATH_KEY = 'MASTER_CONFIG_FILE_PATH'
}
