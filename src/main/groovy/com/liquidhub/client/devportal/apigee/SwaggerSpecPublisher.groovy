package com.liquidhub.client.devportal.apigee

import com.liquidhub.http.client.HTTPInvocationCriteria
import com.liquidhub.http.client.HttpMethod
import com.liquidhub.client.devportal.apigee.config.ApigeeConnectivityConfig
import com.liquidhub.client.devportal.apigee.config.ApigeeConnectivityConfig.DeveloperPortalConfig
import com.liquidhub.client.devportal.apigee.config.ApigeeConnectivityConfig.EdgeServerConfig
import com.liquidhub.client.devportal.apigee.model.SwaggerJsonFile
import com.liquidhub.framework.ci.logger.Logger
import com.liquidhub.framework.ci.logger.PrintStreamLogger

/**
 * Publishes Swagger Specifications to Apigee's Developer Portal using a mix of Apigee Edge Server and Smart Docs API's
 *
 * @author Rahul Mishra, LiquidHub
 *
 */
class SwaggerSpecPublisher {

    private EdgeServerConfig edgeServer

    private DeveloperPortalConfig devPortal

    private String defaultOrg

    //Governs if we are geared to render and publish on developer portal, This behavior can still be changed by clients at runtime
    private boolean autoPublishDocs = true

    private static Logger logger = new PrintStreamLogger(System.out)

    public SwaggerSpecPublisher(ApigeeConnectivityConfig config) {

        if (!config) {
            throw new RuntimeException('Cannot bootstrap SwaggerSpecPublisher without proper configuration, please refer documentation for using this library')
        }

        this.edgeServer = config.edgeServer
        this.devPortal = config.devPortal
        this.defaultOrg = config.organization
        if (!edgeServer.isConfigured()) {
            throw new IllegalStateException('Cannot configure SwaggerSpecPublisher without a proper Edge Server config reference.')
        }

        //IF dev portal config is specified, we are geared to auto publish, the behavior can still be disabled by clients at runtime.
        //This just means - We can publish "IF REQUIRED"
        this.autoPublishDocs = devPortal.isConfigured()

    }

    /**
     * Registers and creates a new revision of the Swagger spec based on the provided file.
     *
     * @param swaggerFile A reference to the swagger file which needs to be published
     * @param orgName The organization name under which the specification will be registered
     *
     *
     */

    public void registerSwaggerSpecRevision(SwaggerJsonFile swaggerFile, orgName = defaultOrg) {

        logger.info "Ready to begin publishing swagger spec into organization $orgName"

        def modelName = swaggerFile.info.name
        def modelDisplayName = swaggerFile.info.displayName
        def modelDescription = swaggerFile.info.description

        createOrUpdateAPIModel(modelName, modelDisplayName, modelDescription, orgName)

        logger.info("Going to work with model name as $modelName")

        swaggerFile.templates.each { template ->
            createOrUpdateModelTemplate(modelName, template, orgName)
        }



        createSwaggerSpecRevision(modelName, swaggerFile, orgName)

        //renderDocs(modelName)

        logger.info 'A new revision of the swagger specification is ready to be rendered and published at developer portal'
    }


    protected String[] createOrUpdateAPIModel(modelName, displayName, description, orgName) {

        def existingAPIModelNames = listAPIModelsInOrg(orgName)

        def httpMethodName, uri

        if (modelName in existingAPIModelNames) {
            //If this is an existing template, update it
            httpMethodName = HttpMethod.PUT
            uri = "/organizations/$orgName/apimodels/$modelName"

            logger.info 'A model with name ' + modelName + ' pre exists. Will update model'

        } else {
            httpMethodName = HttpMethod.POST
            uri = "/organizations/$orgName/apimodels"

            logger.info 'New model detected. Will create API model with name ' + modelName
        }

		println this.edgeServer.baseUrl
		println uri
		println this.edgeServer.username
		println this.edgeServer.password
        return HTTPInvocationCriteria.prepare {
            baseUrl this.edgeServer.baseUrl
            path uri
            credentials(this.edgeServer.username, this.edgeServer.password)
            httpMethod httpMethodName
            jsonBody(['name': modelName, 'displayName': displayName, 'description': description])
        }.send()
    }


    protected void createOrUpdateModelTemplate(modelName, template, orgName) {

        def existingTemplateNames = listTemplatesForModel(modelName, orgName)

        def httpMethod, uri

        if (template.name in existingTemplateNames) {
            //If this is an existing template, update it
            httpMethod = HttpMethod.PUT
            uri = "/organizations/$orgName/apimodels/$modelName/templates/$template.name"

            logger.info 'A template with name ' + template.name + ' pre exists. Will update template'


        } else {
            httpMethod = HttpMethod.POST
            uri = "/organizations/$orgName/apimodels/$modelName/templates"

            logger.info 'New template detected. Will create template with name ' + template.name
        }

        processTemplateForModel(uri, template, httpMethod)
    }


    protected createSwaggerSpecRevision(modelName, swaggerFile, orgName) {

        def uri = "/organizations/$orgName/apimodels/$modelName/import/file"

        logger.info "Registering Swagger Spec Revision for Model at url ${this.edgeServer.baseUrl}/$uri"

        return HTTPInvocationCriteria.prepare {
            baseUrl this.edgeServer.baseUrl
            path uri
            credentials(this.edgeServer.username, this.edgeServer.password)
            httpMethod HttpMethod.POST
            queryParams([format: 'swagger'])
            fileHandle("application/json", swaggerFile.path)
            onSuccess {

                SwaggerSpecPublisher.logger.info "A new swagger specification version has been created successfully"
            }
        }.send()
    }

    protected String[] listAPIModelsInOrg(orgName) {

        def uri = "/organizations/$orgName/apimodels/"

        logger.info "Looking for API Models in current organization at url ${this.edgeServer.baseUrl}/$uri"

        //SwaggerSpecPublisher.logger.info("Username and printing " + this.edgeServer.username + " printing " + this.edgeServer.password)

        return HTTPInvocationCriteria.prepare {
            baseUrl this.edgeServer.baseUrl
            path uri
            credentials(this.edgeServer.username, this.edgeServer.password)
            httpMethod HttpMethod.GET
            onSuccess { response ->
                return response.collect { it.name }//Lets collect all existing API Model Names
            }
        }.send()
    }


    protected String[] listTemplatesForModel(modelName, orgName = defaultOrg) {

        def uri = "/organizations/$orgName/apimodels/$modelName/templates"

        return HTTPInvocationCriteria.prepare {
            baseUrl this.edgeServer.baseUrl
            path uri
            credentials(this.edgeServer.username, this.edgeServer.password)
            httpMethod HttpMethod.GET
            onSuccess { response ->
                return response.collect { it.name }//Lets collect all existing API Model Names
            }
        }.send()
    }


    protected void processTemplateForModel(uri, template, httpMethodName) {

        logger.info "Registering Template for Model at url ${this.edgeServer.baseUrl}/$uri"

        HTTPInvocationCriteria.prepare {
            baseUrl this.edgeServer.baseUrl
            path uri
            credentials(this.edgeServer.username, this.edgeServer.password)
            httpMethod httpMethodName
            queryParams([type: template.type, name: template.name])
            fileHandle("application/html", template.path)
            contentType "HTML"
        }.send()

        logger.info 'Registered Template for Model'
    }

    protected void renderDocs(modelName) {

        logger.info 'Going to submit for rendering of Smart Docs'

        def uri = "/smartdocs/apis/models/$modelName/render"

        HTTPInvocationCriteria.prepare {
            baseUrl this.devPortal.baseUrl
            path uri
            credentials(this.devPortal.username, this.devPortal.password)
            contentType "HTML"
            httpMethod HttpMethod.POST
        }.send()

        logger.info 'Submitted for rendering of Smart Docs'
    }

    protected void publishDocs(modelName) {

        logger.info 'Going to submit for rendering of Smart Docs'

        def uri = "/smartdocs/apis/models/$modelName/render"

        HTTPInvocationCriteria.prepare {
            baseUrl this.devPortal.baseUrl
            path uri
            credentials(this.devPortal.username, this.devPortal.password)
            contentType "HTML"
            httpMethod HttpMethod.POST
        }.send()

        logger.info 'Submitted for rendering of Smart Docs'
    }
}
