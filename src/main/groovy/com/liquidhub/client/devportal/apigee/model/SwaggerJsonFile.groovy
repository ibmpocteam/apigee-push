package com.liquidhub.client.devportal.apigee.model

import groovy.json.JsonSlurper

/**
 * Represents a swagger JSON file and ecanpsulates some parts relevant for deployment to the Apigee Developer Portal
 *
 * @author Rahul Mishra, LiquidHub
 *
 */
class SwaggerJsonFile {

    //The path of the Swagger file in JSON format
    def path

    def content

    //The container for the swagger spec in Developer Portal, all versions of a swagger spec are rendered within the corresponding API Model
    Info info

    // A differentiator which allows us to distinguish between identical swagger specs. This enables clients to distinguish between swagger specs at various stages of development
    def discriminant

    //A reference to all the templates associated the Developer Portal API Model
    def templates = []


    public SwaggerJsonFile(filePath, discriminant=null) {

        assert filePath != null && filePath != "" : "Please provide a swagger specification file path to continue"

        def swaggerSpecInfo

        try {
            this.content = new File(filePath).text
            swaggerSpecInfo = new JsonSlurper().parseText(content).info
        } catch (Exception e) {
            throw new RuntimeException("Failed to load swagger specification specified at " + filePath, e)
        }


        if (!swaggerSpecInfo) {
            throw new RuntimeException("The swagger spec at " + filePath + " is missing the 'info' element")
        }

        this.path = filePath
        this.discriminant = discriminant

        info = new Info(swaggerSpecInfo.title, discriminant?: swaggerSpecInfo.version, swaggerSpecInfo.description)
        if (!info.isValid()) {
            throw new RuntimeException("One of Swagger Spec Info sections - title, version or description is missing in the file at " + filePath + " The elements are used to create the API model.")
        }


    }


    class Template {
        def content, type, name, path
    }

    class Info {

        def name, displayName, description

        public Info(name, qualifier, description) {
            //Capitalize each word of the spring, remove empty spaces and condense
            this.name = name.split(/[^\w]/).collect { it.toLowerCase().capitalize() }.join("")
            this.displayName = name + '-' + qualifier
            this.description = description
        }


        public boolean isValid() {
            name && displayName && description
        }
    }

    public void addTemplate(name, type, filePath) {
        try {
            templates << new Template(name: name, type: type, content: new File(filePath).text, path: filePath)
        } catch (Exception e) {
            throw new RuntimeException("Failed to load template file specified at " + filePath, e)
        }


    }

}
