package com.liquidhub.client.devportal.apigee.config

import com.liquidhub.jenkins.support.credentials.Credentials

class ApigeeConnectivityConfig {

    EdgeServerConfig edgeServer

    DeveloperPortalConfig devPortal

    String organization //The Organization under which the policies and swagger spec are maintained

    class EdgeServerConfig {

        def baseUrl

        Credentials credentials

        public boolean isConfigured() {
            baseUrl &&  credentials.username && credentials.password
        }

        public String getUsername() {
            credentials.username
        }

        public String getPassword() {
            credentials.password
        }

    }

    class DeveloperPortalConfig {

        def baseUrl, credentials, smartDocQueueProcessorUrl, smartDocQueueProcessorParams

        public boolean isConfigured() {
            baseUrl &&  smartDocQueueProcessorUrl && smartDocQueueProcessorParams && credentials.username && credentials.password
        }

        public String getUsername() {
            this.credentials.username
        }

        public String getPassword() {
            this.credentials.password
        }
    }

}
